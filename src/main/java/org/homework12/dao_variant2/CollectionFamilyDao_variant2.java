package org.homework12.dao_variant2;

import org.homework12.creating_entities.Family;
import org.homework12.dao_variant1.FamilyDao;
import org.homework12.dao_variant2.fileService.FileSystemToList;
import org.homework12.exception.FamilyOverflowException;
import org.homework12.logger.LoggerService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao_variant2 implements FamilyDao {

    private final List<Family> families = new ArrayList<>();
    private final String fileName = "families.ser";

    public CollectionFamilyDao_variant2() {
    }

    @Override
    public List<Family> getAllFamilies() {
        LoggerService.info("Retrieving list of families.");
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            LoggerService.info("Retrieving family at index " + index);
            return families.get(index);
        }
        LoggerService.error("Failed to retrieve family at index " + index + ": Index out of bounds.");
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            LoggerService.info("Deleting family at index " + index);
            return true;
        }
        LoggerService.error("Failed to delete family at index " + index + ": Index out of bounds.");
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        boolean removed = families.remove(family);
        if (removed) {
            LoggerService.info("Deleting family: " + family.toString());
        }
        return removed;
    }

    @Override
    public void saveFamily(Family family) {
        int index = families.indexOf(family);

        if (index != -1) {
            families.set(index, family);
            LoggerService.info("Updating family.");
        } else {
            families.add(family);
            LoggerService.info("Adding new family.");
        }
    }

    public boolean saveDataToFile() throws FamilyOverflowException {
        FileSystemToList<Family> familiesFileSystem = new FileSystemToList<>();
        try {
            LoggerService.info("Families saved to file successfully.");
            return familiesFileSystem.recordListToFile(fileName, families);
        } catch (IOException e) {
            LoggerService.error("Error loading data from file: ");
            throw new FamilyOverflowException(e.getMessage());
        }
    }

    @Override
    public void loadData(List<Family> families) throws FamilyOverflowException {
        FileSystemToList<Family> familiesFileSystemToList = new FileSystemToList<>();
        try {
            List<Family> listFromFile = familiesFileSystemToList.getListFromFile(fileName);
            this.families.addAll(listFromFile);
            LoggerService.info("Families loaded from file successfully.");
        } catch (IOException | ClassNotFoundException e) {
            LoggerService.error("Error loading data from file: ");
            throw new FamilyOverflowException(e.getMessage());
        }
    }
}
