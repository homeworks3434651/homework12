package org.homework12;

import org.homework12.dao_variant1.CollectionFamilyDao_variant1;
import org.homework12.dao_variant1.FamilyController_variant1;
import org.homework12.dao_variant1.FamilyService_variant1;
import org.homework12.menu.Menu_variant1;

public class Main_variant1 {

    public static void main(String[] args) {
        CollectionFamilyDao_variant1 familyDAO = new CollectionFamilyDao_variant1();
        FamilyService_variant1 familyService = new FamilyService_variant1(familyDAO);
        FamilyController_variant1 familyController = new FamilyController_variant1(familyService);
        Menu_variant1 menu = new Menu_variant1(familyController);
        menu.console();
    }
}
