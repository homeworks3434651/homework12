package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.TreeSet;

public class UnknownPet extends Pet implements Serializable {

    public UnknownPet(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.UNKNOWN);
    }

    @Override
    public void respond() {
        // Implement abstract method
    }

    @Override
    public void eat() {
        // Implement abstract method
    }
}

