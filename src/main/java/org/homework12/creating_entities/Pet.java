package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public abstract class Pet implements Serializable {
    private transient int age;
    private Species species = Species.UNKNOWN;
    private String nickname;
    private transient int trickLevel;
    private Set<String> habits;

    static {
        System.out.println("Pet class is loaded.");
    }

    {
        System.out.println("A new Pet object is created.");
    }

    public Pet(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = 0;
        this.habits = new LinkedHashSet<>();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        // Convert habits to lowercase and store them
        this.habits = habits.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
    }

    public abstract void respond();

    public abstract void eat();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        return age == pet.age &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(species, pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, nickname, species);
    }

    public String prettyFormat() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        sb.append(String.format("species=%s, ", species != null ? species : "UNKNOWN"));
        sb.append(String.format("nickname='%s', ", nickname != null ? nickname : "null"));
        sb.append(String.format("age=%d, ", age));
        sb.append(String.format("trickLevel=%d, ", trickLevel));

        if (habits != null && !habits.isEmpty()) {
            sb.append(String.format("habits=%s", habits.stream().collect(Collectors.joining(", ", "[", "]"))));
        } else {
            sb.append("habits=null");
        }

        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toString() {
        return String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                species != null ? species : Species.UNKNOWN,
                species.canFly(),
                species.hasFur(),
                species.getNumberOfLegs(),
                nickname != null ? nickname : "null",
                age,
                trickLevel,
                habits != null ? habits.stream().collect(Collectors.joining(", ", "[", "]")) : "null");
    }
}
