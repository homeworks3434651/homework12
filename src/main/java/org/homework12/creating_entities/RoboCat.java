package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.TreeSet;

public class RoboCat extends Pet implements Serializable {
    public RoboCat(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.ROBO_CAT);
    }

    @Override
    public void respond() {
        System.out.println("Beep boop! I'm a robotic cat.");
    }

    @Override
    public void eat() {
        System.out.println("I consume electricity to recharge.");
    }
}
