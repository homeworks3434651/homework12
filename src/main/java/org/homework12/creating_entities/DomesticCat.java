package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.TreeSet;

public class DomesticCat extends Pet implements Foulable, Serializable {

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.DOMESTIC_CAT);
    }

    @Override
    public void respond() {
        System.out.println("Meow! I'm a domestic cat.");
    }

    @Override
    public void eat() {
        System.out.println("I eat cat food.");
    }

    @Override
    public void foul() {
        System.out.println("I just knocked over your favorite plant.");
    }
}
