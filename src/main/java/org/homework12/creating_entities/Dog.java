package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.TreeSet;

public class Dog extends Pet implements Foulable, Serializable {

    public Dog(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>());
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Woof! I'm a dog.");
    }

    @Override
    public void eat() {
        System.out.println("I eat meat.");
    }

    @Override
    public void foul() {
        System.out.println("I left a little surprise for you on the carpet.");
    }
}
