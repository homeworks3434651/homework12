package org.homework12.creating_entities;

import java.io.Serializable;
import java.util.TreeSet;

public class Fish extends Pet implements Serializable {

    public Fish(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void eat() {
        System.out.println("I eat fish food.");
    }
}
