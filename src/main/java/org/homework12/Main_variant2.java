package org.homework12;

import org.homework12.dao_variant2.*;
import org.homework12.menu.Menu_variant2;

public class Main_variant2 {

    public static void main(String[] args) {
        CollectionFamilyDao_variant2 familyDAO = new CollectionFamilyDao_variant2();
        FamilyService_variant2 familyService = new FamilyService_variant2(familyDAO);
        FamilyController_variant2 familyController = new FamilyController_variant2(familyService);
        Menu_variant2 menu = new Menu_variant2(familyController);
        menu.console();
    }
}
