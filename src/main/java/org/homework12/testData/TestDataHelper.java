package org.homework12.testData;

import org.homework12.creating_entities.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static org.homework12.testData.RandomBirthday.getRandomBirthday;

public class TestDataHelper implements Serializable {
    private static final List<String> maleNames = Arrays.asList("John", "Bob", "Charlie", "Ryan", "Alex", "Bryan", "Roy");
    private static final List<String> femaleNames = Arrays.asList("Jane", "Alice", "Lucy", "Debora", "Ruth", "Elizabeth", "Katherina");
    private static final List<String> surnames = Arrays.asList("Doe", "Smith", "Brown", "Peterson", "Jansen", "Johnson", "Jackson", "Jefferson", "Sullivan", "Connor");

    public static final List<String> fatherActivities = Arrays.asList(
            "Reading", "Sports", "Cooking", "Gardening", "Painting", "Coding", "Traveling"
    );
    public static final List<String> motherActivities = Arrays.asList(
            "Shopping", "Yoga", "Movies", "Volunteering", "Painting", "Reading", "Gaming"
    );
    private static final List<String> sonActivities = Arrays.asList(
            "Soccer", "Chess", "Video games", "Cycling", "Drawing", "Music", "Programming"
    );

    private static final List<String> daughterActivities = Arrays.asList(
            "Dance", "Skating", "Reading", "Drawing", "Music", "Cooking", "Painting"
    );

    private static final List<String> dogNames = Arrays.asList("Buddy", "Max", "Charlie", "Rocky", "Daisy", "Molly", "Bailey");
    private static final List<String> catNames = Arrays.asList("Whiskers", "Shadow", "Luna", "Simba", "Cleo", "Milo", "Oscar");
    private static final List<String> fishNames = Arrays.asList("Bubbles", "Nemo", "Goldie", "Splash", "Finn", "Sunny", "Blue");
    private static final List<String> roboCatNames = Arrays.asList("Robo", "Cyber", "Metal", "Sparky", "Byte", "Tech", "Bolt");
    private static final List<String> unknownPetNames = Arrays.asList("Mystery", "Enigma", "X", "Cipher", "Secret", "Phantom", "Ghost");

    private static final List<String> dogHabits = Arrays.asList("Fetch", "Bark", "Play catch");
    private static final List<String> catHabits = Arrays.asList("Purr", "Scratch furniture", "Chase laser pointer");
    private static final List<String> fishHabits = Arrays.asList("Swim in circles", "Hide behind decorations", "Explore the tank");
    private static final List<String> roboCatHabits = Arrays.asList("Charge", "Execute commands", "Sleep in charging dock");

    private static Random random = new Random();

    private static String getRandomName(List<String> names) {
        return names.get(random.nextInt(names.size()));
    }

    static Map<DayOfWeek, List<String>> createRandomSchedule(List<String> activities) {
        int numDays = random.nextInt(8); // Random number of days (0 to 7)
        if (numDays == 0) {
            return null;
        }

        Map<DayOfWeek, List<String>> schedule = new HashMap<>();

        for (int i = 0; i < numDays; i++) {
            DayOfWeek day = DayOfWeek.values()[random.nextInt(DayOfWeek.values().length)];
            int numActivities = random.nextInt(4) + 1; // 1 to 4 activities
            List<String> dayActivities = new ArrayList<>();

            while (dayActivities.size() < numActivities) {
                String activity = activities.get(random.nextInt(activities.size()));
                if (!dayActivities.contains(activity)) {
                    dayActivities.add(activity);
                }
            }

            schedule.put(day, dayActivities);
        }

        return schedule;
    }

    public static Man createRandomFather() {
        String randomMaleName = getRandomName(maleNames);
        String randomFamilySurname = getRandomName(surnames);
        String fatherBirthDay = getRandomBirthday(1970, 1990);
        return new Man(randomMaleName, randomFamilySurname, fatherBirthDay, getRandomIQ());
    }

    public static Woman createRandomMother(Man father) {
        String randomFemaleName = getRandomName(femaleNames);
        String motherBirthDate = createMotherBirthDate(father);
        return new Woman(randomFemaleName, father.getSurname(), motherBirthDate, getRandomIQ());
    }

    public static String createMotherBirthDate(Man father) {
        int plusAge = -2;
        int minusAge = 3;
        String baseBirthDateMother = getRandomBirthDate(father.getBirthDate(), plusAge, minusAge);
        return baseBirthDateMother;
    }

    public static String createChildrenBirthDate(Family family) {
        int minAgeDifference = 30;
        int maxAgeDifference = 19;
        return getRandomBirthDate(family.getMother().getBirthDate(), maxAgeDifference, minAgeDifference);
    }

    public static Man createRandomSon(Family family) {
        List<String> availableMaleNames = new ArrayList<>(maleNames);
        family.getChildren().stream()
                .filter(child -> child instanceof Man)
                .map(Human::getName)
                .forEach(availableMaleNames::remove);

        String randomSonName = getRandomName(availableMaleNames);
        Map<DayOfWeek, List<String>> schedule = createRandomSchedule(sonActivities);
        return new Man(randomSonName, family.getFather().getSurname(), createChildrenBirthDate(family), getRandomIQ(), family, schedule);
    }

    public static Woman createRandomDaughter(Family family) {
        List<String> availableFemaleNames = new ArrayList<>(femaleNames);
        family.getChildren().stream()
                .filter(child -> child instanceof Woman)
                .map(Human::getName)
                .forEach(availableFemaleNames::remove);

        String randomDaughterName = getRandomName(availableFemaleNames);
        Map<DayOfWeek, List<String>> schedule = createRandomSchedule(daughterActivities);
        return new Woman(randomDaughterName, family.getFather().getSurname(), createChildrenBirthDate(family), getRandomIQ(), family, schedule);
    }

    public static int getRandomIQ() {
        return ThreadLocalRandom.current().nextInt(70, 231);
    }

    public static String getRandomBirthDate(String baseBirthDate, int plusAgeDifference, int minusAgeDifference) {
        LocalDate baseDate = LocalDate.parse(baseBirthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        int maxYear = baseDate.getYear() + minusAgeDifference;
        int minYear = baseDate.getYear() + plusAgeDifference;

        return getRandomBirthday(minYear, maxYear);
    }

    public static Pet createRandomPet() {
        int randomPetType = random.nextInt(5); // 0 to 4 representing pet types
        String randomPetName;
        Pet pet;

        switch (randomPetType) {
            case 0:
                randomPetName = getRandomName(dogNames);
                pet = new Dog(randomPetName, random.nextInt(1, 21));
                setPetAttributes(pet, dogHabits);
                return pet;
            case 1:
                randomPetName = getRandomName(catNames);
                pet = new DomesticCat(randomPetName, random.nextInt(1, 16));
                setPetAttributes(pet, catHabits);
                return pet;
            case 2:
                randomPetName = getRandomName(fishNames);
                pet = new Fish(randomPetName, random.nextInt(1, 10));
                setPetAttributes(pet, fishHabits);
                return pet;
            case 3:
                randomPetName = getRandomName(roboCatNames);
                pet = new RoboCat(randomPetName, random.nextInt(31));
                setPetAttributes(pet, roboCatHabits);
                return pet;
            default:
                randomPetName = getRandomName(unknownPetNames);
                return new UnknownPet(randomPetName, random.nextInt(20));
        }
    }

    public static void setPetAttributes(Pet pet, List<String> habits) {
        pet.setTrickLevel(random.nextInt(99) + 1);
        if (!(pet instanceof UnknownPet)) {
            Set<String> petHabits = new HashSet<>(habits);
            pet.setHabits(petHabits);
        }
    }

    public static Set<Pet> createRandomPets() {
        Set<Pet> pets = new HashSet<>();
        int numPets = random.nextInt(7); // Random number of pets (0 to 6)
        for (int i = 0; i < numPets; i++) {
            pets.add(createRandomPet());
        }
        return pets;
    }

    public static void addRandomChildrenToFamily(Family family, int numChildren) {
        for (int j = 0; j < numChildren; j++) {
            if (random.nextBoolean()) {
                family.addChild(createRandomSon(family));
            } else {
                family.addChild(createRandomDaughter(family));
            }
        }
    }

    public static int calculateAge(String birthDate) {
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        Period period = Period.between(birthday, today);
        return period.getYears();
    }

    public static void adjustBirthdatesIfMotherIsOver40(Family family) {
        while (calculateAge(family.getMother().getBirthDate()) > 40) {
            family.getFather().setBirthDate(getRandomBirthday(1970, 1990));
            family.getMother().setBirthDate(createMotherBirthDate(family.getFather()));

            // Adjust birthdate for all children except the last one
            for (int k = 0; k < family.getChildren().size() - 2; k++) {
                family.getChildren().get(k).setBirthDate(createChildrenBirthDate(family));
            }
        }
    }
}
