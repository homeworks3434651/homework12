package org.homework12.testData;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RandomBirthday implements Serializable {
    public static String getRandomBirthday(int minYear, int maxYear) {
        if (maxYear <= minYear) {
            throw new IllegalArgumentException("maxYear must be greater than minYear");
        }

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        LocalDate baseDate = LocalDate.now();
        LocalDate baseYear = baseDate.withYear(ThreadLocalRandom.current().nextInt(minYear, maxYear));

        int dayOfYear = ThreadLocalRandom.current().nextInt(1, baseYear.lengthOfYear());

        LocalDate baseRandBirthday = baseYear.withDayOfYear(dayOfYear);

        LocalDate randDate = LocalDate.of(
                baseRandBirthday.getYear(),
                baseRandBirthday.getMonth(),
                baseRandBirthday.getDayOfMonth()
        );

        return dateFormat.format(randDate);
    }
}
