package org.homework12.testData;

import org.homework12.creating_entities.DayOfWeek;
import org.homework12.dao_variant2.FamilyController_variant2;
import org.homework12.creating_entities.*;
import org.homework12.dao_variant2.FamilyService_variant2;

import java.io.*;
import java.util.*;

public class TestData_variant2 implements Serializable {
    static TestDataHelper testDataHelper = new TestDataHelper();
    private static Random random = new Random();

    public static void addChildByAdoptionOrBirth(Family family, FamilyController_variant2 familyController) {
        if (random.nextBoolean()) {
            Man adoptedSon = TestDataHelper.createRandomSon(family);
            familyController.adoptChild(family, adoptedSon);
        } else {
            Woman adoptedDaughter = TestDataHelper.createRandomDaughter(family);
            familyController.adoptChild(family, adoptedDaughter);
        }
    }

    public static List<Family> runFamilies(FamilyController_variant2 familyController) {
        Random random = new Random();
        int numberFamilies = 10;
        List<Family> createdFamilies = new ArrayList<>();

        // Load previously saved families
        List<Family> existingFamilies = familyController.getAllFamilies();
        if (existingFamilies != null) {
            createdFamilies.addAll(existingFamilies);
        }

        for (int i = createdFamilies.size(); i < numberFamilies; i++) {
            Man randomFather = testDataHelper.createRandomFather();
            Woman randomMother = testDataHelper.createRandomMother(randomFather);
            Family family = new Family(randomFather, randomMother);

            Map<DayOfWeek, List<String>> scheduleFather = testDataHelper.createRandomSchedule(TestDataHelper.fatherActivities);
            randomFather.setSchedule(scheduleFather);
            Map<DayOfWeek, List<String>> scheduleMother = testDataHelper.createRandomSchedule(TestDataHelper.motherActivities);
            randomMother.setSchedule(scheduleMother);

            int numChildren = random.nextInt(4);
            testDataHelper.addRandomChildrenToFamily(family, numChildren);

            if (i % 4 == 0) {
               addChildByAdoptionOrBirth(family, familyController);
            } else if (i % 2 == 0) {
                familyController.bornChild(family, FamilyService_variant2.getRandomMaleName(), FamilyService_variant2.getRandomFemaleName());
                testDataHelper.adjustBirthdatesIfMotherIsOver40(family);
            }

            family.setPets(testDataHelper.createRandomPets());
            familyController.saveFamily(family);
            createdFamilies.add(family);
        }

        return createdFamilies;
    }
}
