package org.homework12.dao_variant1;

import org.homework12.exception.FamilyOverflowException;
import org.homework12.creating_entities.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    void loadData(List<Family> families)  throws FamilyOverflowException;
}
