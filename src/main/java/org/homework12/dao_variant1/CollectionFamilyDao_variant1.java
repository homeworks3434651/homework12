package org.homework12.dao_variant1;

import org.homework12.creating_entities.Family;
import org.homework12.exception.FamilyOverflowException;
import org.homework12.logger.LoggerService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao_variant1 implements FamilyDao {

    private final List<Family> families = new ArrayList<>();
    private final String fileName = "family_data.ser";

    public CollectionFamilyDao_variant1() {
    }

    @Override
    public List<Family> getAllFamilies() {
        LoggerService.info("Retrieving list of families.");
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            LoggerService.info("Retrieving family at index " + index);
            return families.get(index);
        }
        LoggerService.error("Failed to retrieve family at index " + index + ": Index out of bounds.");
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            LoggerService.info("Deleting family at index " + index);
            return true;
        }
        LoggerService.error("Failed to delete family at index " + index + ": Index out of bounds.");
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        boolean removed = families.remove(family);
        if (removed) {
            LoggerService.info("Deleting family: " + family.toString());
        }
        return removed;
    }

    @Override
    public void saveFamily(Family family) {
        int index = families.indexOf(family);

        if (index != -1) {
            families.set(index, family);
            LoggerService.info("Updating family.");
        } else {
            families.add(family);
            LoggerService.info("Adding new family.");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void loadData(List<Family> families) throws FamilyOverflowException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            List<Family> loadedFamilies = (List<Family>) ois.readObject();
            families.addAll(loadedFamilies);
            LoggerService.info("Families loaded from file successfully.");
        } catch (IOException | ClassNotFoundException e) {
            LoggerService.error("Error loading families from file: " + e.getMessage());
        }
    }

    public boolean saveDataToFile() throws FamilyOverflowException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(families);
            LoggerService.info("Families saved to file successfully.");
            return true;
        } catch (IOException e) {
            LoggerService.error("Error saving data to file: " + e.getMessage());
            return false;
        }
    }
}
