package org.homework12.dao_variant1;

import org.homework12.exception.FamilyOverflowException;
import org.homework12.creating_entities.*;

import java.time.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FamilyService_variant1 {

    private final CollectionFamilyDao_variant1 familyDao;

    public final static long currentTimeMillis = System.currentTimeMillis();
    public final static LocalDate currentDate = Instant.ofEpochMilli(currentTimeMillis)
            .atZone(ZoneId.systemDefault())
            .toLocalDate();

    public FamilyService_variant1(CollectionFamilyDao_variant1 familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        List<Family> allFamilies = Optional.ofNullable(familyDao.getAllFamilies())
                .orElseGet(List::of);

        if (allFamilies.isEmpty()) {
            System.out.println("The list of families is empty.");
        }

        return allFamilies;
    }

    public List<Family> getAllFamilies(Predicate<Family> predicate) {
        return getAllFamilies().stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = getAllFamilies();
        for (int i = 0; i < allFamilies.size(); i++) {
            System.out.println("Family " + (i + 1) + ":");
            System.out.println(allFamilies.get(i).prettyFormat());
        }
    }

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        if (getAllFamilies().isEmpty()) {
            System.out.println("The list of families is empty.");
            return false;
        }
        return familyDao.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        if (family == null) {
            System.out.println("Cannot save null family.");
            return;
        }

        if (getAllFamilies().isEmpty()) {
            familyDao.saveFamily(family);
            System.out.println("Family added successfully.");
        } else if (getAllFamilies().contains(family)) {
            // Check if any child is already part of another family
            for (Human child : family.getChildren()) {
                if (child.getFamily() != null && !child.getFamily().equals(family)) {
                    System.out.println("Family didn't update.");
                    return;
                } else {
                    familyDao.saveFamily(family);
                    System.out.println("Family updated successfully.");
                }
            }
        } else {
            familyDao.saveFamily(family);
            System.out.println("Family added successfully.");
        }
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        return getAllFamilies(family -> family.countFamily() > numberOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        return getAllFamilies(family -> family.countFamily() < numberOfMembers);
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return getAllFamilies(family -> family.countFamily() == numberOfMembers).size();
    }

    public Family createNewFamily(Man father, Woman mother) {
        Family newFamily = new Family(father, mother);
        getAllFamilies().add(newFamily);
        saveFamily(newFamily);
        return newFamily;
    }

    public void deleteFamilyByIndex(int index) {
        if (index >= 0 && index < getAllFamilies().size()) {
            getAllFamilies().remove(index);
        } else {
            System.out.printf("There's no %d in the family list.%n", index);
        }
    }

    public static final String[] MALE_NAMES = {"Michael", "David", "Matthew", "Andrew", "Christopher", "Daniel", "Joseph"};
    public static final String[] FEMALE_NAMES = {"Jennifer", "Emily", "Jessica", "Amanda", "Megan", "Sophia", "Olivia"};

    public static int calculateInheritedIQ(Man father, Woman mother, Human child) {
        validateAndAdjustIQ(father, mother);

        int fatherIQ = father.getIQ();
        int motherIQ = mother.getIQ();

        // If the child's IQ is set, return that value
        if (child != null && child.getIQ() > 0 && child.getIQ() != 110) {
            return child.getIQ();
        }

        return (fatherIQ + motherIQ) / 2;
    }

    private static void validateAndAdjustIQ(Man father, Woman mother) {
        if (father.getIQ() == 0 && mother.getIQ() == 0) {
            System.out.println("Error: At least one parent's IQ must be specified.");
            father.setIQ(110);
            mother.setIQ(110);
            return;
        }

        if (father.getIQ() == 0) {
            System.out.println("Warning: Father's IQ is not specified. Using mother's IQ for the child.");
            father.setIQ(mother.getIQ());
        }

        if (mother.getIQ() == 0) {
            System.out.println("Warning: Mother's IQ is not specified. Using father's IQ for the child.");
            mother.setIQ(father.getIQ());
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        if (family == null) {
            System.out.println("Warning: Family is not specified. Creating a new family for the child.");
        }

        Man father = family.getFather();
        Woman mother = family.getMother();
        Human child = new Human();

        if (father == null || mother == null) {
            System.out.println("Error: Father or mother is missing in the family.");
            return null;
        }

        if (child == null) {
            System.out.println("Error: Child can't be null.");
            return null;
        }

        String childName;

        // Determine the gender randomly (50% chance for Man or Woman)
        boolean isMale = new Random().nextBoolean();

        int inheritedIQ = calculateInheritedIQ(family.getFather(), family.getMother(), child);

        if (isMale) {
            childName = (maleName != null && !maleName.isEmpty()) ? maleName : getRandomMaleName();
            family.addChild(new Man(childName, father.getSurname(), family, inheritedIQ));
        } else {
            childName = (femaleName != null && !femaleName.isEmpty()) ? femaleName : getRandomFemaleName();
            family.addChild(new Woman(childName, father.getSurname(), family, inheritedIQ));
        }
        saveFamily(family);
        return family;
    }

    private static final Random random = new Random();

    public static String getRandomMaleName() {
        return MALE_NAMES[random.nextInt(MALE_NAMES.length)];
    }

    public static String getRandomFemaleName() {
        return FEMALE_NAMES[random.nextInt(FEMALE_NAMES.length)];
    }

    public Family adoptChild(Family family, Human child) {
        if (getAllFamilies().contains(family)) {
            // Check if the child is already part of another family

            if (child.getFamily() != null && !child.getFamily().equals(family)) {
                System.out.println("Error: This person is already part of another family.");
                System.out.println("Child adoption failed.");
                return null;
            }
            child.setSurname(family.getFather().getSurname());
            family.addChild(child);
            saveFamily(family);
            return family;
        } else {
            System.out.println("Error: Family not found.");
            System.out.println("Child adoption failed.");
            return null;
        }
    }

    public void deleteAllChildrenOlderThan(int age) {
        getAllFamilies().forEach(family -> {
            List<Human> childrenToRemove = family.getChildren().stream()
                    .filter(child -> calculateAge(child) > age)
                    .collect(Collectors.toList());

            childrenToRemove.forEach(family::deleteChild);
            saveFamily(family);
        });
    }

    private int calculateAge(Human human) {
        LocalDate birthDate = Instant.ofEpochMilli(human.getBirthDateUnix())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        Period ageDifference = Period.between(birthDate, currentDate);
        return ageDifference.getYears();
    }

    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (index >= 0 && index < getAllFamilies().size()) {
            if (getAllFamilies().get(index) != null) {
                Family existingFamily = getAllFamilies().get(index);
                return existingFamily;
            } else {
                return null;
            }
        } else {
            System.out.printf("Invalid index: %d. Family not found.%n", index);
            return null;
        }
    }

    public Set<Pet> getPets(int index) {
        return index >= 0 && index < getAllFamilies().size()
                ? Optional.ofNullable(getAllFamilies().get(index))
                .map(Family::getPets)
                .orElseGet(Collections::emptySet)
                : Collections.emptySet();
    }

    public void addPet(int index, Set<Pet> pet) {
        if (index >= 0 && index < getAllFamilies().size()) {
            if (getFamilyById(index) != null) {
                Family family = getAllFamilies().get(index);
                family.setPets(pet);
                saveFamily(family);
            } else {
                System.out.println("Family at index " + index + " is null. Cannot add pet.");
            }
        } else {
            System.out.println("Invalid index: " + index + ". Cannot add pet.");
        }
    }

    public void loadData(List<Family> families) {
        try {
            familyDao.loadData(families);
        } catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage() + " FamilyOverflowException");
        }
    }

    public boolean saveDataToFile() {
        try {
            return familyDao.saveDataToFile();
        } catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage() + " FamilyOverflowException");
            return false;
        }
    }
}
