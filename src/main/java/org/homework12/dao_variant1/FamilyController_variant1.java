package org.homework12.dao_variant1;

import org.homework12.exception.FamilyOverflowException;
import org.homework12.creating_entities.*;
import org.homework12.menu.Menu_variant1;

import java.util.List;
import java.util.Set;

public class FamilyController_variant1 {
    private final FamilyService_variant1 familyService;

    public FamilyController_variant1(FamilyService_variant1 familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies(family -> true);
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        List<Family> result = familyService.getFamiliesBiggerThan(numberOfMembers);
        printResult(result, "Families with numbers of members bigger than " + numberOfMembers + ":\n");
        return result;
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        List<Family> result = familyService.getFamiliesLessThan(numberOfMembers);
        printResult(result, "Families with numbers of members less than " + numberOfMembers + ":\n");
        return result;
    }

    public void printResult(List<Family> result, String conditionMessage) {
        if (result.isEmpty()) {
            System.out.println("No families found satisfying the condition.");
        } else {
            System.out.println(conditionMessage);

            List<Family> allFamilies = getAllFamilies();
            for (Family family : result) {
                int index = allFamilies.indexOf(family) + 1;
                System.out.println("Family " + index + ":");
                System.out.println(family.prettyFormat());
            }
        }
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numberOfMembers);
    }

    public Family createNewFamily(Man father, Woman mother) {
        return familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public void checkFamilySize(Family family) {
        int familySize = family.countFamily();
        if (familySize > Menu_variant1.MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException("Family size exceeds the allowed limit");
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        checkFamilySize(family);
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human child) {
        checkFamilySize(family);
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public void printFamilyById(Family family, int index) {
        if (getFamilyById(index) != null) {
            System.out.println(family.toString().replace("family:\n", "Family: " + (index + 1) + "\n"));
        }
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Set<Pet> pet) {
        familyService.addPet(index, pet);
    }

    public boolean saveDataToFile() {
        return familyService.saveDataToFile();
    }

    public void loadData(List<Family> families) {
        familyService.loadData(families);
        for (Family family : families) {
            System.out.println(family);
        }
    }
}
