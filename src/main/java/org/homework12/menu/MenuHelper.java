package org.homework12.menu;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuHelper {

    public static final Scanner scanner = new Scanner(System.in);

    public void displayMenu() {
        System.out.println("\n1. Load previous saved data");
        System.out.println("2. Display all families");
        System.out.println("3. Display families with more members than specified");
        System.out.println("4. Display families with fewer members than specified");
        System.out.println("5. Count families with a specific number of members");
        System.out.println("6. Create a new family");
        System.out.println("7. Delete a family by index");
        System.out.println("8. Edit a family by index");
        System.out.println("9. Delete all children older than a specified age");
        System.out.println("0. Exit\n");
    }

    public int getDaysInMonth(int year, int month) {
        if (month == 2) {
            // Check if it's a leap year
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                return 29; // Leap year
            } else {
                return 28; // Non-leap year
            }
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30; // April, June, September, November have 30 days
        } else {
            return 31; // January, March, May, July, August, October, December have 31 days
        }
    }

    public String promptValidName(String message) {
        while (true) {
            try {
                String input = promptString(message);
                validateName(input);
                return formatName(input);
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public void validateName(String name) {
        if (name.matches(".*\\d.*")) {
            throw new IllegalArgumentException("Name cannot contain digits.");
        }

        if (!name.matches("^[a-zA-Z]+(-[a-zA-Z]+)*(\\s[a-zA-Z]+(-[a-zA-Z]+)*)*$")) {
            throw new IllegalArgumentException("Name must contain only latin letters, hyphens, and spaces.");
        }
    }

    public String formatName(String name) {
        String[] words = name.split("[\\s-]+");
        StringBuilder formattedName = new StringBuilder();
        for (String word : words) {
            if (formattedName.length() > 0) {
                formattedName.append(" ");
            }
            formattedName.append(word.substring(0, 1).toUpperCase())
                    .append(word.substring(1).toLowerCase());
        }
        return formattedName.toString();
    }

    public String promptGender(String message) {
        while (true) {
            try {
                System.out.print(message + ": ");
                String gender = scanner.nextLine().trim();

                if (gender.equalsIgnoreCase("Man") || gender.equalsIgnoreCase("Woman")) {
                    return gender;
                } else {
                    throw new IllegalArgumentException("Invalid gender. Please enter 'Man' or 'Woman'.");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public String promptString(String message) {
        System.out.print(message + ": ");
        return scanner.nextLine();
    }

    public int promptInt(String message, int minValue, int maxValue) {
        int value;
        do {
            value = validateInput(message + " (" + minValue + " - " + maxValue + "): ", minValue, maxValue);
        } while (value < minValue || value > maxValue);

        return value;
    }

    public int validateInput(String prompt, int minValue, int maxValue) {
        int value;
        while (true) {
            try {
                System.out.print(prompt);
                while (!scanner.hasNextInt()) {
                    System.out.print("Invalid input. Please enter a valid integer: ");
                    scanner.next(); // consume the invalid input
                }
                value = scanner.nextInt();
                scanner.nextLine(); // consume the newline

                if (value < minValue || value > maxValue) {
                    System.out.println("Value must be between " + minValue + " and " + maxValue + ".");
                } else {
                    break; // valid input, exit the loop
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid integer.");
                scanner.nextLine(); // consume the invalid input
            }
        }
        return value;
    }

    public void handleInputMismatchException() {
        System.out.println("Error: Invalid input. Please enter a valid integer.");
        scanner.nextLine();
    }

    public void handleIllegalArgumentException(String message) {
        System.out.println("Error: " + message);
    }
}
