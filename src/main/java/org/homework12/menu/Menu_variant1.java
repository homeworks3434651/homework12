package org.homework12.menu;

import org.homework12.dao_variant1.FamilyController_variant1;
import org.homework12.exception.FamilyOverflowException;
import org.homework12.creating_entities.*;
import org.homework12.testData.TestData_variant1;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu_variant1 implements Serializable {
    public static final int MAX_FAMILY_SIZE = 7;
    private final FamilyController_variant1 familyController;
    private static final Scanner scanner = new Scanner(System.in);
    MenuHelper menuDisplayHelper = new MenuHelper();

    public Menu_variant1(FamilyController_variant1 familyController) {
        this.familyController = familyController;
    }

    public void console() {
        TestData_variant1.runFamilies(familyController);
        familyController.saveDataToFile();
        boolean exit = false;
        while (!exit) {
            menuDisplayHelper.displayMenu();
            try {
                System.out.print("Enter your choice: ");
                int choice = scanner.nextInt();
                scanner.nextLine();
                switch (choice) {
                    case 1:
                        familyController.loadData(familyController.getAllFamilies());
                        break;
                    case 2:
                        familyController.displayAllFamilies();
                        break;
                    case 3:
                        getFamiliesBiggerOrLessThan(true);
                        break;
                    case 4:
                        getFamiliesBiggerOrLessThan(false);
                        break;
                    case 5:
                        countFamiliesWithMemberNumber();
                        break;
                    case 6:
                        createFamilyWithUserInput();
                        break;
                    case 7:
                        deleteFamilyByIndex();
                        break;
                    case 8:
                        editFamilyAtIndex();
                        break;
                    case 9:
                        deleteChildrenOlderThan();
                        break;
                    case 0:
                        exit = true;
                        familyController.saveDataToFile();
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid integer.");
                scanner.nextLine();
            }
        }
        familyController.saveDataToFile();
        scanner.close();
    }

    public void getFamiliesBiggerOrLessThan(boolean isBigger) {
        while (true) {
            try {
                System.out.print("Enter the number of members: ");
                int numberOfMembers = scanner.nextInt();
                scanner.nextLine();

                if (numberOfMembers < 0) {
                    throw new IllegalArgumentException("Number of members cannot be negative.");
                }

                if (isBigger) {
                    familyController.getFamiliesBiggerThan(numberOfMembers);
                } else {
                    familyController.getFamiliesLessThan(numberOfMembers);
                }

                // Break out of the loop if the operation is successful
                break;
            } catch (InputMismatchException e) {
                menuDisplayHelper.handleInputMismatchException();
            } catch (IllegalArgumentException e) {
                menuDisplayHelper.handleIllegalArgumentException(e.getMessage());
            }
        }
    }

    public void countFamiliesWithMemberNumber() {
        while (true) {
            try {
                System.out.print("Enter the number of members: ");
                int numberOfMembers = scanner.nextInt();
                scanner.nextLine();

                if (numberOfMembers < 0) {
                    throw new IllegalArgumentException("Number of members cannot be negative.");
                }

                System.out.println("There are " + familyController.countFamiliesWithMemberNumber(numberOfMembers) +
                        " families with " + numberOfMembers + " members in the family.\n");

                // Break out of the loop if the operation is successful
                break;
            } catch (InputMismatchException e) {
                menuDisplayHelper.handleInputMismatchException();
            } catch (IllegalArgumentException e) {
                menuDisplayHelper.handleIllegalArgumentException(e.getMessage());
            }
        }
    }

    public void createFamilyWithUserInput() {
        try {
            String motherName = menuDisplayHelper.promptValidName("Enter the name of the mother");
            String motherSurname = menuDisplayHelper.promptValidName("Enter the surname of the mother");
            int motherBirthYear = menuDisplayHelper.promptInt("Enter the birth year of the mother", 1970, 2006);
            int motherBirthMonth = menuDisplayHelper.promptInt("Enter the birth month of the mother", 1, 12);
            int motherBirthDay = menuDisplayHelper.promptInt("Enter the birth day of the mother", 1, menuDisplayHelper.getDaysInMonth(motherBirthYear, motherBirthMonth));
            int motherIQ = menuDisplayHelper.promptInt("Enter the IQ of the mother", 70, 230);

            String fatherName = menuDisplayHelper.promptValidName("Enter the name of the father");
            String fatherSurname = menuDisplayHelper.promptValidName("Enter the surname of the father");
            int fatherBirthYear = menuDisplayHelper.promptInt("Enter the birth year of the father", 1970, 2005);
            int fatherBirthMonth = menuDisplayHelper.promptInt("Enter the birth month of the father", 1, 12);
            int fatherBirthDay = menuDisplayHelper.promptInt("Enter the birth day of the father", 1, menuDisplayHelper.getDaysInMonth(fatherBirthYear, fatherBirthMonth));
            int fatherIQ = menuDisplayHelper.promptInt("Enter the IQ of the father", 70, 230);

            String formattedFatherBirthDate = String.format("%02d/%02d/%d", fatherBirthDay, fatherBirthMonth, fatherBirthYear);
            Man father = new Man(fatherName, fatherSurname, formattedFatherBirthDate, fatherIQ);

            String formattedMotherBirthDate = String.format("%02d/%02d/%d", motherBirthDay, motherBirthMonth, motherBirthYear);
            Woman mother = new Woman(motherName, motherSurname, formattedMotherBirthDate, motherIQ);

            familyController.createNewFamily(father, mother);
            System.out.println("New family: ");
            familyController.printFamilyById(familyController.getFamilyById(familyController.getAllFamilies().size() - 1), familyController.getAllFamilies().size() - 1);
        } catch (InputMismatchException e) {
            System.out.println("Error: Invalid input. Please enter valid data.");
            scanner.nextLine();
        }
    }

    public void deleteFamilyByIndex() {
        while (true) {
            try {
                System.out.print("Enter the index of the family to delete (or -1 to exit): ");

                if (!scanner.hasNext()) {
                    break;  // exit the loop if there's no more input
                }

                int inputIndex = scanner.nextInt();

                if (inputIndex == -1) {
                    break;  // exit the loop if -1 is entered
                }

                if (inputIndex <= 0 || inputIndex > familyController.getAllFamilies().size()) {
                    throw new IllegalArgumentException("Invalid index. Please enter a valid index.");
                }

                Family deletedFamily = familyController.getFamilyById(inputIndex - 1); // Adjusting to 0-based index
                if (deletedFamily != null) {
                    familyController.deleteFamily(inputIndex - 1);  // Delete by 0-based index
                    System.out.println("Family with index " + inputIndex + " deleted.");
                } else {
                    System.out.println("Error: Family with index " + inputIndex + " not found.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid index (integer).");
                scanner.nextLine(); // Consume newline to avoid an infinite loop
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public int editFamilyAtIndex() {
        int editFamilyIndex = -2;
        while (true) {
            System.out.print("Enter the index of the family to edit (or -1 to exit): ");
            try {
                editFamilyIndex = scanner.nextInt();
                scanner.nextLine();

                if (editFamilyIndex == -1) {
                    return -1;  // exit the method if -1 is entered
                }

                if (editFamilyIndex <= 0 || editFamilyIndex > familyController.getAllFamilies().size()) {
                    throw new IllegalArgumentException("Invalid index. Please enter a valid index.");
                }

                Family familyToEdit = familyController.getFamilyById(editFamilyIndex - 1);
                displayEditFamilyMenu(familyToEdit, editFamilyIndex);
                return editFamilyIndex;  // exit the loop after successfully displaying the menu
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid index (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    public void displayEditFamilyMenu(Family familyToEdit, int editFamilyIndex) {
        while (true) {
            System.out.println("1. Give birth to a child");
            System.out.println("2. Adopt a child");
            System.out.println("3. Return to the main menu");
            System.out.print("Enter your choice: ");

            int editChoice;
            try {
                editChoice = scanner.nextInt();
                scanner.nextLine();

                if (familyToEdit.countFamily() == MAX_FAMILY_SIZE) {
                    throw new FamilyOverflowException("Family size exceeds the allowed limit");
                }

                switch (editChoice) {
                    case 1:
                        editFamilyByBirth(familyToEdit, editFamilyIndex);
                        break;
                    case 2:
                        editFamilyByAdoption(familyToEdit, editFamilyIndex);
                        break;
                    case 3:
                        return; // Return to the main menu
                    default:
                        System.out.println("Invalid choice. Please enter a valid choice.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid choice (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            } catch (FamilyOverflowException e) {
                System.out.println("Error: " + e.getMessage());
                return;
            }
        }
    }

    public void editFamilyByBirth(Family familyToEdit, int editFamilyIndex) {
        String boyName = menuDisplayHelper.promptValidName("Enter the name for the child (boy)");
        String girlName = menuDisplayHelper.promptValidName("Enter the name for the child (girl)");
        familyController.bornChild(familyToEdit, boyName, girlName);
        familyController.saveFamily(familyToEdit);
        System.out.println("Editing family: ");
        familyController.printFamilyById(familyController.getFamilyById(editFamilyIndex - 1), editFamilyIndex - 1);
    }

    public void editFamilyByAdoption(Family familyToEdit, int editFamilyIndex) {
        String childGender = menuDisplayHelper.promptGender("Enter the gender of the child (Man/Woman)");
        String childName = menuDisplayHelper.promptValidName("Enter the name of the child");
        int childBirthYear = menuDisplayHelper.promptInt("Enter the birth year of the child", 2009, 2024);
        int childIQ = menuDisplayHelper.promptInt("Enter the IQ of the child", 70, 230);
        if (childGender.equalsIgnoreCase("Man")) {
            Man son = new Man(childName, familyToEdit.getFather().getSurname(), childBirthYear, childIQ, familyToEdit);
            familyController.adoptChild(familyToEdit, son);
        } else if (childGender.equalsIgnoreCase("Woman")) {
            Woman daughter = new Woman(childName, familyToEdit.getFather().getSurname(), childBirthYear, childIQ, familyToEdit);
            familyController.adoptChild(familyToEdit, daughter);
        }
        familyController.saveFamily(familyToEdit);
        System.out.println("Editing family: ");
        familyController.printFamilyById(familyController.getFamilyById(editFamilyIndex - 1), editFamilyIndex - 1);
    }

    public void deleteChildrenOlderThan() {
        while (true) {
            try {
                System.out.print("Enter the age to delete children older than (or -1 to exit): ");
                int ageDeleteAllChildrenOlderThan = scanner.nextInt();
                scanner.nextLine();

                if (ageDeleteAllChildrenOlderThan == -1) {
                    break;  // exit the loop if -1 is entered
                }

                if (ageDeleteAllChildrenOlderThan < 0) {
                    throw new IllegalArgumentException("Age cannot be negative.");
                }

                familyController.deleteAllChildrenOlderThan(ageDeleteAllChildrenOlderThan);
                System.out.println("Children older than " + ageDeleteAllChildrenOlderThan + " have been deleted.");
                break;  // exit the loop after successful deletion
            } catch (InputMismatchException e) {
                System.out.println("Error: Invalid input. Please enter a valid age (integer).");
                scanner.nextLine();
            } catch (IllegalArgumentException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
}
