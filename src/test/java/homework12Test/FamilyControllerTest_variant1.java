package homework12Test;

import org.homework12.creating_entities.*;
import org.homework12.dao_variant1.*;
import org.homework12.exception.FamilyOverflowException;
import org.homework12.menu.Menu_variant1;
import org.junit.jupiter.api.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyControllerTest_variant1 {

    private final InputStream originalSystemIn = System.in;
    private ByteArrayInputStream inputStream;
    private ByteArrayOutputStream outputStream;
    private final String fileName = "family_data.ser";

    @BeforeEach
    public void setUp() {
        redirectSystemInput("1\n9\n");
    }

    @AfterEach
    public void tearDown() {
        restoreSystemInput();
        File testFile = new File(fileName);
        if (testFile.exists()) {
            testFile.delete();
        }
    }

    private void restoreSystemInput() {
        System.setIn(originalSystemIn);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();
    FamilyController_variant1 controller = createFamilyController();

    public void setSystemInput(String input) {
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public FamilyController_variant1 createFamilyController() {
        CollectionFamilyDao_variant1 collectionFamilyDao = new CollectionFamilyDao_variant1();
        FamilyService_variant1 familyService = new FamilyService_variant1(collectionFamilyDao);
        return new FamilyController_variant1(familyService);
    }

    private void redirectSystemInput(String input) {
        inputStream = new ByteArrayInputStream(input.getBytes());
        outputStream = new ByteArrayOutputStream();
        System.setIn(inputStream);
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    void testGetAllFamilies() {
        controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                   new Woman("Jane", "Sullivan", "12/12/1970"));
        controller.saveFamily(family1);
        controller.saveFamily(family2);
        assertNotNull(controller.getAllFamilies());
        assertEquals(3, controller.getAllFamilies().size());
    }

    @Test
    void testGetFamilyByIndex() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));
        Family retrievedFamily = controller.getFamilyByIndex(0);
        assertNotNull(retrievedFamily);
        assertEquals(family, retrievedFamily);
    }

    @Test
    void testDeleteFamilyByIndex() {
        controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                   new Woman("Jane", "Sullivan", "12/12/1970"));
        assertTrue(controller.deleteFamily(0));
        assertEquals(0, controller.getAllFamilies().size());
    }

    @Test
    void testDeleteFamily() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));
        assertTrue(controller.deleteFamily(family));
        assertEquals(0, controller.getAllFamilies().size());
    }

    @Test
    void testSaveFamily() {
        Family family = new Family(new Man("Lewis", "Smith", "10/10/1970"), new Woman("Jacklin", "Smith", "12/12/1970"));
        controller.saveFamily(family);
        Family retrievedFamily = controller.getFamilyByIndex(0);
        assertNotNull(retrievedFamily);
        assertEquals("Lewis", retrievedFamily.getFather().getName());
        assertEquals("Smith", retrievedFamily.getFather().getSurname());
        assertEquals("Jacklin", retrievedFamily.getMother().getName());
    }

    @Test
    public void testUpdateExistingFamily() {
        controller.saveFamily(family1);

        // Update the existing family with the same members
        family1.getFather().setBirthDate("22/01/1980");
        family1.getMother().setBirthDate("31/07/1985");

        // Call saveFamily() method with the updated family
        controller.saveFamily(family1);

        // Verify the result (existing family should be updated, not added)
        assertEquals(1, controller.getAllFamilies().size());
        assertTrue(controller.getAllFamilies().contains(family1));
    }

    @Test
    void testDisplayAllFamilies() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        controller.saveFamily(family1);

        // Redirect output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        // Call displayAllFamilies
        controller.displayAllFamilies();

        // Verify output
        String expectedOutput = "Family 1:\n" + family.prettyFormat() + "\nFamily 2:\n" + family1.prettyFormat() + "\n";
        assertEquals(expectedOutput.trim().replaceAll("\\s", ""), outputStream.toString().trim().replaceAll("\\s", ""));
    }

    @Test
    void testGetFamiliesBiggerThan() {
        family1.addChild(new Man("John", "Doe", "01/01/2000"));
        family1.addChild(new Man("Jack", "Doe", "01/01/2001"));
        family2.addChild(new Woman("Jane", "Smith", "01/01/2002"));
        controller.saveFamily(family1);
        controller.saveFamily(family2);

        List<Family> biggerFamilies = controller.getFamiliesBiggerThan(3);
        assertEquals(1, biggerFamilies.size());
        assertTrue(biggerFamilies.contains(family1));
        assertFalse(biggerFamilies.contains(family2));
    }

    @Test
    void testGetFamiliesLessThan() {
        family1.addChild(new Man("John", "Doe", "01/01/2000"));
        controller.saveFamily(family1);
        controller.saveFamily(family2);

        List<Family> lessFamilies = controller.getFamiliesLessThan(3);
        assertEquals(1, lessFamilies.size());
        assertTrue(lessFamilies.contains(family2));
    }

    @Test
    void testCreateNewFamily() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));
        assertEquals("John", family.getFather().getName());
        assertEquals("Sullivan", family.getFather().getSurname());
        assertEquals("Jane", family.getMother().getName());
    }

    @Test
    void testBornChild() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        assertEquals(0, family.getChildren().size());

        family = controller.bornChild(family, "Jack", "Jill");
        assertNotNull(family);
        assertEquals(1, family.getChildren().size());

        Human child = family.getChildren().get(0);
        assertNotNull(child);
        assertNotNull(child.getName());
        assertFalse(child.getName().isEmpty());
        assertEquals("Sullivan", child.getSurname());
        assertEquals(family, child.getFamily());
        assertTrue(child instanceof Man || child instanceof Woman);
    }

    @Test
    void testAdoptChild() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        Man child = new Man("Jack", "", "23/11/2012");
        family = controller.adoptChild(family, child);
        assertNotNull(family);
        assertEquals(1, family.getChildren().size());
        assertEquals("Jack", family.getChildren().get(0).getName());
    }

    @Test
    void testCheckFamilySize() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        assertTrue(family.countFamily() < Menu_variant1.MAX_FAMILY_SIZE);

        // Add more members to the family until it overflows
        for (int i = 0; i < Menu_variant1.MAX_FAMILY_SIZE; i++) {
            family.addChild(new Man("Child" + i, "Doe", "01/01/2015"));
        }

        assertThrows(FamilyOverflowException.class, () -> controller.checkFamilySize(family));
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        family1.addChild(new Man("John", "Doe", "01/01/2000"));
        family2.addChild(new Woman("Jane", "Smith", "01/01/2002"));
        controller.saveFamily(family1);
        controller.saveFamily(family2);

        assertEquals(2, controller.countFamiliesWithMemberNumber(3));
        assertEquals(0, controller.countFamiliesWithMemberNumber(2));
    }

    @Test
    public void testDeleteFamilyByIndexVoid() {
        controller.saveFamily(family1);
        controller.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method
        controller.deleteFamilyByIndex(0);

        // Verify the result and printed output
        assertEquals(1, controller.getAllFamilies().size());
        assertEquals(family2, controller.getAllFamilies().get(0));

        String expectedOutput = ""; // Since the family is successfully deleted, no message should be printed
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void testDeleteFamilyByInvalidIndexVoid() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method with an invalid index
        controller.deleteFamilyByIndex(2);

        // Restore the original System.out
        System.setOut(System.out);

        // Verify the printed output for an invalid index
        String expectedOutput = "The list of families is empty.\n" +
                                "There's no 2 in the family list.";
        assertEquals(expectedOutput.trim().replaceAll("\\s", ""), outContent.toString().trim().replaceAll("\\s", ""));
    }


    @Test
    void testDeleteAllChildrenOlderThan() {
        family1.addChild(new Man("Jack", "Doe", "01/01/2000"));
        family1.addChild(new Man("John", "Doe", "01/01/2005"));
        family1.addChild(new Woman("Jane", "Doe", "01/01/2020"));

        controller.saveFamily(family1);
        controller.deleteAllChildrenOlderThan(16);

        assertEquals(1, family1.getChildren().size());
        assertEquals("Jane", family1.getChildren().get(0).getName());
    }

    @Test
    void testCount() {
        controller.saveFamily(family1);
        controller.saveFamily(family2);
        assertEquals(2, controller.count());
    }

    @Test
    void testGetFamilyById() {
        controller.saveFamily(family1);
        controller.saveFamily(family2);

        assertEquals(family1, controller.getFamilyById(0));
        assertEquals(family2, controller.getFamilyById(1));
        assertNull(controller.getFamilyById(2));
    }

    @Test
    void testPrintFamilyById() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        // Redirect output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        controller.printFamilyById(family, 0);

        // Verify output
        String expectedOutput = "Family: 1\n" + family.prettyFormat() + "\n";
        assertEquals(expectedOutput.trim().replaceAll("\\s", ""), outputStream.toString().trim().replaceAll("\\s", ""));
    }

    @Test
    void testGetPets() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        Dog dog = new Dog( "Max", 2);
        DomesticCat cat = new DomesticCat("Whiskers", 1);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);
        family.setPets(pets);
        controller.saveFamily(family);

        assertEquals(pets, controller.getPets(0));
    }

    @Test
    void testAddPet() {
        Family family = controller.createNewFamily(new Man("John", "Sullivan", "10/10/1970"),
                                                   new Woman("Jane", "Sullivan", "12/12/1970"));

        Dog dog = new Dog( "Max", 2);
        DomesticCat cat = new DomesticCat("Whiskers", 1);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);

        controller.addPet(0, pets);
        controller.saveFamily(family);

        // Verify if pets are added
        assertEquals(pets, controller.getPets(0));
    }

    @Test
    void testSaveDataToFile() {
        controller.saveFamily(family1);
        controller.saveFamily(family2);
        controller.saveDataToFile();

        File file = new File("family_data.ser");
        assertTrue(file.exists());
        assertTrue(controller.saveDataToFile());
        file.delete();
    }

    @Test
    void testLoadData() {
        List<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family2);

        // Call loadData
        controller.loadData(families);

        // Redirect output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        // Verify if the loaded families are printed to the console
        String expectedOutput = "";
        for (Family family : families) {
            expectedOutput += family.toString() + "\n";
        }
        controller.loadData(families);
        assertEquals(expectedOutput.trim().replaceAll("\\s", ""), outputStream.toString().trim().replaceAll("\\s", ""));
    }
}
