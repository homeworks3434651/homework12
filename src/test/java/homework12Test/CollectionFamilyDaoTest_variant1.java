package org.homework12.dao_variant1;

import homework12Test.TestUtilities;
import org.homework12.creating_entities.Family;
import org.homework12.exception.FamilyOverflowException;
import org.homework12.testData.TestData_variant1;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class CollectionFamilyDaoTest_variant1 {

    private CollectionFamilyDao_variant1 familyDao;
    private List<Family> testFamilies;
    private final String fileName = "family_data.ser";

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    @BeforeEach
    void setUp() {
        familyDao = new CollectionFamilyDao_variant1();
        testFamilies = new ArrayList<>();
    }

    @AfterEach
    void tearDown() {
        File testFile = new File(fileName);
        if (testFile.exists()) {
            testFile.delete();
        }
    }

    @Test
    void testGetAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();
        assertNotNull(allFamilies);
        assertEquals(0, allFamilies.size());

        allFamilies.add(family1);
        allFamilies.add(family2);
        assertEquals(2, allFamilies.size());
    }

    @Test
    void testSaveFamily() {
        familyDao.saveFamily(family1);
        familyDao.saveFamily(family2);
        List<Family> allFamilies = familyDao.getAllFamilies();
        assertEquals(2, allFamilies.size());
        assertEquals(family1, allFamilies.get(0));
    }

    @Test
    void testDeleteFamilyByIndex() {
        familyDao.saveFamily(family1);
        assertTrue(familyDao.deleteFamily(0));
        List<Family> allFamilies = familyDao.getAllFamilies();
        assertEquals(0, allFamilies.size());
    }

    @Test
    void testDeleteFamily() {
        familyDao.saveFamily(family1);
        assertTrue(familyDao.deleteFamily(family1));
        List<Family> allFamilies = familyDao.getAllFamilies();
        assertEquals(0, allFamilies.size());
    }

    @Test
    void testGetFamilyByIndex() {
        familyDao.saveFamily(family1);
        assertEquals(family1, familyDao.getFamilyByIndex(0));
    }

    @Test
    void testSaveDataToFile() {
        familyDao.saveFamily(family1);
        familyDao.saveFamily(family2);
        familyDao.saveDataToFile();
        File file = new File("family_data.ser");
        assertTrue(file.exists());
        assertTrue(familyDao.saveDataToFile());
    }

    @Test
    void testLoadData() {
        try {
            List<Family> families = TestData_variant1.runFamilies(new FamilyController_variant1(new FamilyService_variant1(familyDao)));
            familyDao.saveDataToFile();
            familyDao.loadData(families);
            assertEquals(families.size(), familyDao.getAllFamilies().size());
        } catch (FamilyOverflowException e) {
            fail("Exception should not be thrown: " + e.getMessage());
        }
    }
}
